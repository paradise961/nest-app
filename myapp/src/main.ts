import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import { join } from 'path';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  const environment = process.env.NODE_ENV || 'development';
  console.log('Enviroment', environment)
  if (environment === 'production'){
    app.use(express.static(join(process.cwd(), 'client/build')));
  }
  
  await app.listen(3001);
}
bootstrap();
