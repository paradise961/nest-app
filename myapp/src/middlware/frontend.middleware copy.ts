// // import { ExpressMiddleware, Middleware, NestMiddleware } from '@nestjs/common';
// import { Injectable, NestMiddleware } from '@nestjs/common';
// import * as path from 'path';
// import { Request, Response } from 'express';
// // import { join } from 'path';

// const allowedExt = [
//     '.js',
//     '.ico',
//     '.css',
//     '.png',
//     '.jpg',
//     '.woff2',
//     '.woff',
//     '.ttf',
//     '.svg',
//     '.json',
//     '.chunk.css'
// ];

// const resolvePath = (file: string) => path.resolve(`../myreact/build//${file}`);

// @Injectable()
// export class FrontendMiddleware implements NestMiddleware {
//     use(req: Request, res: Response, next: Function) {
        
//             const { url } = req;
//             if (url.indexOf('api') === 1) {
//                 console.log('Request POPCORN...');
//                 // it starts with /api --> continue with execution
//                 next();
//             } else if (allowedExt.filter(ext => url.indexOf(ext) > 0).length > 0) {
//                 console.log('Requestsini');
//                 // it has a file extension --> resolve the file
//                 res.sendFile(resolvePath(url));
//             } else {
//                 console.log('Request 2...');
//                 // in all other cases, redirect to the index.html!
//                 res.sendFile(resolvePath('index.html'));
//             }
//             // next();
//     }



// }


// export class ServeHTMLMiddleware implements NestMiddleware {
//     use(req: Request, res: Response, next: () => void) {
//       // here you can check if the requested path is your api endpoint, if that's the case then we have to return next()
//       if (req.path.includes('graphql')) {
//         return next();
//       }
  
//       // change the path to the correct html page path in your project
//       res.sendFile(join(process.cwd(), '../client/dist/index.html'));
//     }
//   }