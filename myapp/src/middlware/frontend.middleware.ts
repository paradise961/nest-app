// import { ExpressMiddleware, Middleware, NestMiddleware } from '@nestjs/common';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { join } from 'path';
import { Request, Response } from 'express';

@Injectable()
export class FrontendMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: () => void) {
        const environment = process.env.NODE_ENV || 'development';
        if (environment === 'development'){
            return next();
        }
        // Only production enviroment
        console.log(req.baseUrl);
        if (req.baseUrl.includes('api')) {
            console.log('Enter API ZONE')
            return next();
        }

        res.sendFile(join(process.cwd(), 'client/build/index.html'));
    }
}