import { Controller, Get, Req, Res, Next } from '@nestjs/common';
import { AppService } from './app.service';
import * as path from 'path';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  onlyInDev(): string {
    return 'show only in dev';
  }

  @Get('api')
  myAPI(): string {
    return 'my API goes here';
  }

}
