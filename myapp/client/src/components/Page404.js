import React, { Component } from 'react';
import { Link } from "react-router-dom";

class Page404 extends Component {
    render() {
        return(
            <div>
                <p>404 Page not found</p>
                <p>The requested url 
                    <span className="red-text"> {this.props.location.pathname} </span> 
                    was not found
                </p>
                <Link to="/" className="btn waves-effect waves-light">
                    Back to Home
                </Link>
            </div>
        );
    }
}

export default Page404;