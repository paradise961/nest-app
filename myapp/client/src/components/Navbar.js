import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import styled from 'styled-components';

class Navbar extends Component {
    render() {
        return (
            <React.Fragment>
                <NavWrapper>
                    <div className="nav-wrapper">
                    <a href="/" className="brand-logo">MyReact</a>
                    <a href="#!" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                    <ul className="right hide-on-med-and-down">
                        <li><Link to="/page1">Page1</Link></li>
                        <li><Link to="/page2">Page2</Link></li>
                    </ul>
                    </div>
                </NavWrapper>
                <ul className="sidenav" id="mobile-demo">
                    <li><Link to="/page1">Page1</Link></li>
                    <li><Link to="/page2">Page2</Link></li>
                </ul>
            </React.Fragment>
        );
    }
}
const NavWrapper = styled.nav`
    background: #2a2a72;
    .nav-wrapper {
        margin-left: 15%;
        margin-right: 15%;
    }
`
export default Navbar