import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Navbar from './components/Navbar';
import LandingPage from './components/LandingPage';
import Page1 from './components/Page1';
import Page2 from './components/Page2';
import Page404 from './components/Page404';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <BrowserRouter>
          <Navbar />
          <div className="container">
            <Switch>
              <Route exact path="/" component={LandingPage} />
              <Route exact path="/page1" component={Page1} />
              <Route exact path="/page2" component={Page2} />
              <Route component={Page404} />
            </Switch>
          </div>
        </BrowserRouter>
      </React.Fragment>
    );  
  }
}

export default App;
